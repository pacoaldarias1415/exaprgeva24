package exaprgeva24;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Exa24Arrays {

  public static boolean numero(String s) {
    Pattern p = Pattern.compile("[0-9]+"); //1
    Matcher m = p.matcher(s);
    return m.matches();
  }

  public static void main(String arg[]) {

    Map<String, String> hm = new HashMap<String, String>(); //2

    hm.put("1", "123");
    hm.put("2", "123a");
    hm.put("3", "456");

    Iterator it = hm.entrySet().iterator(); //3

    while (it.hasNext()) {
      Map.Entry e = (Map.Entry) it.next(); //4
      String clave = (String) e.getKey(); //5
      String valor = (String) e.getValue(); //6
      System.out.println(clave + " " + valor + " " + numero(valor));
    }

  }
}

/* EJECUCION;
 3 456 true
 2 123a false
 1 123 true
 */
