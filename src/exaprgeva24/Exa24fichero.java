package exaprgeva24;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

class articulo
        implements Serializable { //1

  protected String id;
  protected String nombre;

  public articulo(String id_, String nombre_) {
    id = id_;
    nombre = nombre_;
  }

  public String getId() {
    return id;
  }

  public String getNombre() {
    return nombre;
  }
}

public class Exa24fichero {

  public static void main(String[] args)
          throws Exception {
    String fichero = "articulos.bin";
    String[] ids = {"1", "2"};
    String nombres[] = {"articulo1", "articulo2"};

    ObjectOutputStream w = null;
    try {
      w = new ObjectOutputStream(new FileOutputStream(fichero)); //2
      for (int i = 0; i < ids.length; i++) {
        articulo a = new articulo(ids[i], nombres[i]); //3
        w.writeObject(a);  // 4
      }
    } catch (Exception e) {
    } finally {
      w.close();
    }

    ObjectInputStream r = null;
    try {
      r = new ObjectInputStream(new FileInputStream(fichero)); //5

      articulo a = null;
      a = (articulo) r.readObject();  //6
      while (a != null) { //7
        System.out.println(a.getId() + " " + a.getNombre());
        a = (articulo) r.readObject(); //8
      }
    } catch (Exception e) {
    } finally {
      r.close();
    }
  }
}
/* EJECUCION:
 1 articulo1
 2 articulo2
 */
