package exaprgeva24;

/**
 * ******************************
 */
public class Exa24Clases
        extends e //1
        implements i //2
{

  int d;

  public Exa24Clases() {
    super(3);
    this.d = 2;
  }

  public void setD(int d_) {
    d = d_;
  }

  public static void main(String[] args) {
    int i = 5;  // 3
    Exa24Clases x = new Exa24Clases();
    x.modifica(i);
    x.setD(i);
    x.mostrar(); // 4
  }

  public void modifica(int x) {
    x = 2;
  }

  private void mostrar() {
    System.out.println(d);
  }
}

/**
 * ******************************
 */
interface i { //5

  void modifica(int x);
}

/**
 * ******************************
 */
class e { //6

  int d;

  public e(int d_) {
    d = d_;
  }

  private void mostrar() {
    System.out.println(d);
  }
}
/*
 * 5
 */
